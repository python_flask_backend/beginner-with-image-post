import cv2
import requests

# 後端的POST URL，基本上改掉localhost就好了
url = 'http://localhost:5000/image/face'

# 這邊只是測試用，儲存的圖片原始格式是cv2使用的格式
img = cv2.imread('./image1.jpeg')

# 使用cv2轉成jpg的圖片格式
imencoded = cv2.imencode(".jpg", img)[1]

# 設定POST的BODY內容
file = {'image': ('image.jpg', imencoded.tostring(), 'image/jpeg', {'Expires': '0'})}

# 送出POST。並讀取回傳值
response = requests.post(url, files=file, timeout=5)
print(response)
