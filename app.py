from flask import Flask

app = Flask(__name__)
app.config.from_object('config')

from controller.image_con import mod as imageModule
app.register_blueprint(imageModule)


if __name__ == '__main__':
    app.run()
