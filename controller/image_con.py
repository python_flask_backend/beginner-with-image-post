import cv2
import numpy
from flask import Blueprint, request
from flask_api import status

from agent import id_generator as ig
from app import app

mod = Blueprint('image_controller', __name__, url_prefix='/image')

@mod.route('/face', methods=['POST'])
def postUserImage():
    image = request.files['image'].read()

    # numpy轉碼
    npimg = numpy.fromstring(image, numpy.uint8)

    # 轉譯，這邊就可以拿出來用了
    img = cv2.imdecode(npimg, cv2.IMREAD_COLOR)

    # 儲存用，先產生ID及路徑再使用cv2.imwrite
    imageId = ig.generateId('faceImage') + '.jpg'
    imagePath = app.config['UPLOAD_FOLDER_IMAGE'] + imageId
    cv2.imwrite(imagePath, img)

    return 'OK', status.HTTP_200_OK
