import os

_basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = True
THREADED = True

UPLOAD_FOLDER_IMAGE = "./face_image/"
